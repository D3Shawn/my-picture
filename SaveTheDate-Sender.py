from twilio.rest import Client
import xlrd, xlwt
from xlutils.copy import copy # http://pypi.python.org/pypi/xlutils

client = Client("AC71d15a0515225abd855fbc9bb2c3afaf", "8db1795d78549cedb86491c411a637df")
# Reading an excel file using Python 


# Give the location of the file 
# loc = ("My Half Invitation List  (version 2).xls") 
loc = ("c:/dev/Text/De's Numbers Save the Date.xls") 
# loc = ("c:/dev/Text/testnumbers.xls")
# To open Workbook 
wb = xlrd.open_workbook(loc) 
writebook= copy(wb)
writesheet= writebook.get_sheet(0)
sheet = wb.sheet_by_index(0) 
media_link = "https://gitlab.com/D3Shawn/my-picture/-/raw/master/wedding%20Invitation.PNG"

msg = '''%s, We pray that you will be able to celebrate with us on our special day. Please visit the link below to RSVP for the wedding! You will need to use the password to access the site. We ask that you RSVP by July 8th. If we have no response from you by then, we will assume that you are unable to attend. Regardless, we ask for your prayers as we begin our journey together as husband and wife. 

https://jonesdaniel2020.wixsite.com/mysite/details-about-the-big-day

*Please note: In light of COVID-19 concerns, we will be checking temperatures before attendees enter the ceremony. We ask that if you are not feeling well, to please join us via livestream. We also ask that all in-person attendees bring a mask to wear.

RSVP Password: jd2020
*We are kindly asking that you do not share this password with anyone else.*

Date: August 1, 2020

Time: 5:00 PM

Location: Bridlewood of Madison, 3024 MS-22, Madison, MS 39110 

https://goo.gl/maps/FU2BXJ7MTM4yGnA59

Registry: https://jonesdaniel2020.wixsite.com/mysite/wedding-registry

This is an automated message, please do not reply to this message. 
'''

# for y in range(4):
for y in range(sheet.nrows-1):
    #(Roz,Columb)
    #Phone Number
    x = sheet.cell_value(y+1, 1) 
    #Names
    z = sheet.cell_value(y+1, 0)
    
    if sheet.cell_value(y+1,3) == "Sent":
        pass
    else:
        try:
            
            client.messages.create(to=x , from_="+12066811524", body=msg %z, media_url= media_link)
            writesheet.write(y+1,3,"Sent")
            print(x,z)
        except:
            writesheet.write(y+1,3,"Not Sent")   
    writebook.save(loc)

print("Done")